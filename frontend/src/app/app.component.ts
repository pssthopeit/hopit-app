import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {MatFormFieldModule, MatSelectionList} from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {Form} from '@angular/forms';
import {forEach} from '@angular/router/src/utils/collection';
import {RequestOptions} from '@angular/http';
@Component({
  selector: 'file-upload',
  template: '<input type="file" [multiple]="multiple" #fileInput>'
})
export class FileUploadComponent {
  @Input() multiple: boolean = false;
  @ViewChild('fileInput') inputEl: ElementRef;
  formData: FormData;

  constructor(private http: HttpClient) {
  }

  upload() {
    let inputEl: HTMLInputElement = this.inputEl.nativeElement;
    let fileCount: number = inputEl.files.length;
    this.formData = new FormData();
    if (fileCount > 0) { // a file was selected
      for (let i = 0; i < 1; i++) {
        this.formData.append('multipartFile', inputEl.files.item(i));
      }
    }
  }
  getData(): FormData {
    return this.formData;
  }
}
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild(MatSelectionList)
  selectList: MatSelectionList;
  @ViewChild(FileUploadComponent)
  file: FileUploadComponent;
  title = 'app';
  name: string;
  password: string;
  login: boolean;
  user: any;
  error: boolean;
  token: string;
  donations: ExampleDataSource;
  numberR: number;
  message: string;
  selectedUsers: any[];
  users: any[];
  newUsername: string;
  newPassword: string;
  rNewPassword: string;
  email: string;


  constructor(private http: HttpClient) {
    this.login = true;
    this.numberR = 1;
    this.donations = new ExampleDataSource(http);
    this.users = users;
  }
  displayedColumns = ['position', 'username', 'amount', 'donationDate'];
  log(): void {
    this.http.post('http://localhost:8081/login', 'username=' + this.name + '&password=' + this.password, {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'), withCredentials: true } ).
    subscribe(
        data => { this.user = data; this.login = false; this.token = data['token']; }, error => {this.login = true; this.error = true; });
  }
  register() {
    if (this.newPassword === this.rNewPassword) {
      let obj = {};
      obj['username'] = this.newUsername;
      obj['password'] = this.newPassword;
      obj['email'] = this.email;
      this.http.post('http://localhost:8081/register', obj, {withCredentials: true}).subscribe(value => {
        this.newPassword = null;
        this.newUsername = null;
        this.rNewPassword = null;
        this.email = null;
      });
    } else {
      this.error = true;
    }
  }
  selectAll() {
    this.selectList.selectAll();
  }
  deselectAll() {
    this.selectList.deselectAll();
  }
  sendMsg() {
    let dataToSend: FormData = this.file.getData();
    if (!dataToSend) {
      dataToSend = new FormData();
    }
    const ids: string[] = this.selectList.selectedOptions.selected.map(data => data.value);
    let value = ids[0];
    for (let i = 1; i < ids.length; i++) {
      value += ',' + ids[i];
    }
    dataToSend.set(`msg`, this.message);
    dataToSend.set(`ids`, value);
    let headers: Headers;
    headers = new Headers({});
    this.http.post(`http://localhost:8081/user/message`, dataToSend, { withCredentials: true }).subscribe(value => {
     this.file.formData = null;
     this.selectList.deselectAll();
     this.message = ``;
    });
  }
}

const donations = [{'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}, {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'donationDate': new Date().getTime(), 'amount': '123123', 'user': {'id': Math.floor(Math.random() * (1000 - 1 + 1)) + 1, 'username': 'test'}}];
const users = donations.map(value => value['user']);

export class ExampleDataSource extends DataSource<any> {
  constructor(private http: HttpClient) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    return this.getData();
  }

  disconnect() {
  }
  getData(): Observable<any[]> {
    return this.http.get('http://localhost:8081/user/donations', {withCredentials: true});
  }
}
