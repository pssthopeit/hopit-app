package pl.psst.hopeitapp.backend.domain.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.springframework.data.repository.cdi.Eager;
import pl.psst.hopeitapp.backend.domain.base.BaseEntity;
import pl.psst.hopeitapp.backend.domain.message.Message;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hi_user")
public class User extends BaseEntity {

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "token")
    private String token;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_msg",
    joinColumns = @JoinColumn(name = "id_user", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "id_msg", referencedColumnName = "id"))
    private List<Message> messages;



    public static final class UserBuilder {
        Long id;
        private String username;
        private String password;
        private String token;
        private List<Message> messages;

        private UserBuilder() {
        }

        public static UserBuilder anUser() {
            return new UserBuilder();
        }

        public UserBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public UserBuilder withUsername(String username) {
            this.username = username;
            return this;
        }

        public UserBuilder withPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder withToken(String token) {
            this.token = token;
            return this;
        }

        public UserBuilder withMessages(List<Message> messages) {
            this.messages = messages;
            return this;
        }

        public User build() {
            User user = new User();
            user.setId(id);
            user.setUsername(username);
            user.setPassword(password);
            user.setToken(token);
            user.setMessages(messages);
            return user;
        }
    }
}
