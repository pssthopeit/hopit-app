package pl.psst.hopeitapp.backend.domain.converters;

import pl.psst.hopeitapp.backend.domain.user.User;
import pl.psst.hopeitapp.backend.domain.user.UserDetails;
import pl.psst.hopeitapp.shared.dtos.user.UserDTO;
import pl.psst.hopeitapp.shared.dtos.user.UserDetailsDTO;

public class Users {

    protected Users() {
    }

    public static UserDTO toUserDTO(User user, UserDetails userDetails) {
        return UserDTO.builder().id(user.getId()).username(user.getUsername())
            .userDetails(toUserDetailsDTO(userDetails)).token(user.getToken()).build();
    }

    public static UserDTO toUserDTO(User user) {
        return UserDTO.builder().id(user.getId()).username(user.getUsername())
            .token(user.getToken()).build();
    }

    public static UserDetailsDTO toUserDetailsDTO(UserDetails userDetails) {
        return UserDetailsDTO.builder().id(userDetails.getId()).address(userDetails.getAddress())
            .email(userDetails.getEmail()).firstName(userDetails.getFirstName())
            .lastName(userDetails.getLastName()).build();
    }

    public static User toUser(UserDTO userDTO) {
        return User.UserBuilder.anUser().withUsername(userDTO.getUsername())
            .withPassword(userDTO.getPassword()).build();
    }

    public static UserDetails toUserDetails(UserDetailsDTO userDetailsDTO, User user) {
        return UserDetails.UserDetailsBuilder.anUserDetails()
            .withAddress(userDetailsDTO.getAddress()).withEmail(userDetailsDTO.getEmail())
            .withFirstName(userDetailsDTO.getFirstName()).withLastName(userDetailsDTO.getLastName())
            .withUser(user).build();
    }
}
