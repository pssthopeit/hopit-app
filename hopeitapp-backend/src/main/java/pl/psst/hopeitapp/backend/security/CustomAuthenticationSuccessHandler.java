package pl.psst.hopeitapp.backend.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import pl.psst.hopeitapp.backend.domain.converters.Users;
import pl.psst.hopeitapp.backend.domain.user.User;
import pl.psst.hopeitapp.backend.security.user.SecurityUser;
import pl.psst.hopeitapp.backend.services.UserService;
import pl.psst.hopeitapp.shared.dtos.user.UserDTO;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Component @RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final UserService userService;


    @Override public void onAuthenticationSuccess(HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse, Authentication authentication)
        throws IOException, ServletException {
        HttpSession httpSession = httpServletRequest.getSession();
        SecurityUser authUser =
            (SecurityUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String sessionId = httpServletRequest.getSession(false).getId();
        httpSession.setAttribute("username", authUser.getUsername());
        httpSession.setAttribute("authorities", authUser.getAuthorities());

        httpServletResponse.setStatus(HttpServletResponse.SC_OK);

        try {
            updateResponse(httpServletResponse, authUser, sessionId);
        } catch (NotFound notFound) {
            httpServletResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

    }

    private void saveUser(pl.psst.hopeitapp.backend.domain.user.User user) throws NotFound {
        userService.updateUser(user);
    }

    private void updateResponse(HttpServletResponse httpServletResponse, SecurityUser authUser,
        String sessionId) throws IOException, NotFound {

        deleteCookieSid(httpServletResponse, sessionId);

        User user = User.UserBuilder.anUser()
                .withUsername(authUser.getUsername()).withPassword(authUser.getPassword())
                .withToken(sessionId).build();

        saveUser(user);

        UserDTO userDTO = Users.
            toUserDTO(userService.getUserByUsernameAndPassword(user.getUsername(), user.getPassword()));

        ObjectMapper objectMapper = new ObjectMapper();
        httpServletResponse.setHeader("X-Auth-Token", sessionId);
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
        httpServletResponse.getWriter().write(objectMapper.writeValueAsString(userDTO));
        httpServletResponse.getWriter().flush();
        httpServletResponse.getWriter().close();

    }

    private void deleteCookieSid(HttpServletResponse httpServletResponse, String sessionId) {
        Cookie cookie = new Cookie("JSESSIONID", sessionId);
        cookie.setMaxAge(6666666);
        httpServletResponse.addCookie(cookie);
    }
}
