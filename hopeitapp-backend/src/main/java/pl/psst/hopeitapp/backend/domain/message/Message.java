package pl.psst.hopeitapp.backend.domain.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.psst.hopeitapp.backend.domain.base.BaseEntity;
import pl.psst.hopeitapp.backend.domain.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "hi_message")
public class Message  extends BaseEntity{

    @Column(name = "msg")
    private String msg;
    @ManyToMany(mappedBy = "messages")
    private List<User> users;
    private LocalDateTime localDateTime;
    private byte[] image;

    @PrePersist
    private void prePersist(){
        this.localDateTime = LocalDateTime.now();
    }


    public static final class MessageBuilder {
        Long id;
        private String msg;
        private List<User> users;

        private MessageBuilder() {
        }

        public static MessageBuilder aMessage() {
            return new MessageBuilder();
        }

        public MessageBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public MessageBuilder withMsg(String msg) {
            this.msg = msg;
            return this;
        }

        public MessageBuilder withUsers(List<User> users) {
            this.users = users;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.setId(id);
            message.setMsg(msg);
            message.setUsers(users);
            return message;
        }
    }
}
