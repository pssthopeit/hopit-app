package pl.psst.hopeitapp.backend.domain.converters;

import pl.psst.hopeitapp.backend.domain.donations.Donation;
import pl.psst.hopeitapp.backend.domain.user.User;
import pl.psst.hopeitapp.shared.dtos.user.DonationDTO;

import java.util.Date;

public class Donations {

    protected Donations() {
    }

    public static Donation fromDonationDTO(DonationDTO donationDTO, User user) {
        return Donation.DonationBuilder.aDonation().withAmmount(donationDTO.getAmount())
            .withDate(new Date()).withUser(user).build();
    }

    public static DonationDTO toDonationDTO(Donation donation) {
        return DonationDTO.builder().amount(donation.getAmount()).donationDate(donation.getDate())
            .id(donation.getId()).userDTO(Users.toUserDTO(donation.getUser())).build();
    }
}

