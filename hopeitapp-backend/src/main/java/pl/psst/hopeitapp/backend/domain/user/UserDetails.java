package pl.psst.hopeitapp.backend.domain.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.psst.hopeitapp.backend.domain.base.BaseEntity;
import pl.psst.hopeitapp.backend.domain.base.UserType;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hi_user_details")
public class UserDetails extends BaseEntity{

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "user_type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private UserType userType;

    @OneToOne
    @JoinColumn(name = "id_user")
    private User user;



    public static final class UserDetailsBuilder {
        Long id;
        private String firstName;
        private String lastName;
        private String email;
        private String address;
        private UserType userType;
        private User user;

        private UserDetailsBuilder() {
        }

        public static UserDetailsBuilder anUserDetails() {
            return new UserDetailsBuilder();
        }

        public UserDetailsBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public UserDetailsBuilder withFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserDetailsBuilder withLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserDetailsBuilder withEmail(String email) {
            this.email = email;
            return this;
        }

        public UserDetailsBuilder withAddress(String address) {
            this.address = address;
            return this;
        }

        public UserDetailsBuilder withUserType(UserType userType) {
            this.userType = userType;
            return this;
        }

        public UserDetailsBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public UserDetails build() {
            UserDetails userDetails = new UserDetails();
            userDetails.setId(id);
            userDetails.setFirstName(firstName);
            userDetails.setLastName(lastName);
            userDetails.setEmail(email);
            userDetails.setAddress(address);
            userDetails.setUserType(userType);
            userDetails.setUser(user);
            return userDetails;
        }
    }
}
