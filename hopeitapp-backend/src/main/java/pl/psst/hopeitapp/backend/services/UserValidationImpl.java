package pl.psst.hopeitapp.backend.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import pl.psst.hopeitapp.backend.domain.repositories.UserRepository;
import pl.psst.hopeitapp.backend.domain.user.User;
import pl.psst.hopeitapp.shared.dtos.validators.UserValidation;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserValidationImpl implements UserValidation {

    private final UserRepository userRepository;

    @Override
    public boolean exists(String username) {
        return userRepository.exists(Example.of(User.UserBuilder.anUser().withUsername(username).build()));
    }
}
