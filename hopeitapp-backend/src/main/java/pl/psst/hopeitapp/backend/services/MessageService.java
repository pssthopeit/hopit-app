package pl.psst.hopeitapp.backend.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.psst.hopeitapp.backend.domain.converters.Messages;
import pl.psst.hopeitapp.backend.domain.repositories.MessageRepository;
import pl.psst.hopeitapp.backend.domain.repositories.UserRepository;
import pl.psst.hopeitapp.shared.dtos.user.MessageDTO;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessageService {

    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    private final pl.psst.hopeitapp.backend.domain.converters.Messages message;


    public List<MessageDTO> getUserMessages(Long userId){
        return userRepository.getOne(userId).getMessages().stream().map(Messages::toMessageDTO).collect(
            Collectors.toList());
    }

    public void saveUserMessage(String msg, MultipartFile file, String ids) throws IOException {
        messageRepository.save(message.toMessage(msg, file, ids));
    }
}
