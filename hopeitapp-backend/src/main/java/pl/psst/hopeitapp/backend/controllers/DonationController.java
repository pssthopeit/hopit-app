package pl.psst.hopeitapp.backend.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.psst.hopeitapp.backend.services.DonationService;
import pl.psst.hopeitapp.shared.dtos.user.DonationDTO;

import java.util.List;


@RestController @RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DonationController {

    private final DonationService donationService;


    @GetMapping(path = "/{userId}/donations", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDonations(@PathVariable("userId") Long userId) {
        List<DonationDTO> donations = donationService.getAllDonations(userId);
        return donations != null ?
            ResponseEntity.ok(donations) :
            (ResponseEntity<?>) ResponseEntity.notFound();
    }

    @GetMapping(path = "/donations", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDonations() {
        List<DonationDTO> donations = donationService.getAllDonations();
        return donations != null ?
            ResponseEntity.ok(donations) :
            (ResponseEntity<?>) ResponseEntity.notFound();
    }

    @PostMapping(path = "/{userId}/donations", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> makeDonation(@PathVariable("userId") Long userId,
        @RequestBody DonationDTO donationDTO) {
        DonationDTO donation = donationService.saveDonation(donationDTO, userId);
        return donation != null ? ResponseEntity.ok(donation) :
            (ResponseEntity<?>) ResponseEntity.notFound();
    }
}
