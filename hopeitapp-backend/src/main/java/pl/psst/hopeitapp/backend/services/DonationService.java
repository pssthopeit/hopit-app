package pl.psst.hopeitapp.backend.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.psst.hopeitapp.backend.domain.converters.Donations;
import pl.psst.hopeitapp.backend.domain.repositories.DonationRepository;
import pl.psst.hopeitapp.backend.domain.repositories.UserRepository;
import pl.psst.hopeitapp.backend.domain.user.User;
import pl.psst.hopeitapp.shared.dtos.user.DonationDTO;

import java.util.List;
import java.util.stream.Collectors;

@Service @RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DonationService {

    private final DonationRepository donationRepository;
    private final UserRepository userRepository;


    public List<DonationDTO> getAllDonations(Long userId) {
        return donationRepository.findAllByUser_Id(userId).stream()
            .map(Donations::toDonationDTO)
            .collect(Collectors.toList());
    }
    public List<DonationDTO> getAllDonations() {
        return donationRepository.findAll().stream()
            .map(Donations::toDonationDTO)
            .collect(Collectors.toList());
    }

    public DonationDTO saveDonation(DonationDTO donationDTO, Long userId){
        User user = userRepository.getOne(userId);
        return Donations.toDonationDTO(
            donationRepository.save(Donations.fromDonationDTO(donationDTO, user)));
    }

}
