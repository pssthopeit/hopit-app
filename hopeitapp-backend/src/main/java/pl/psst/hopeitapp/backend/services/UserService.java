package pl.psst.hopeitapp.backend.services;

import lombok.RequiredArgsConstructor;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.psst.hopeitapp.backend.domain.repositories.UserRepository;
import pl.psst.hopeitapp.backend.domain.user.User;
import pl.psst.hopeitapp.backend.security.user.SecurityUser;
import pl.psst.hopeitapp.shared.dtos.RegisterUserDto;
import pl.psst.hopeitapp.shared.dtos.user.UserDTO;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserService {

  private final UserRepository userRepository;

  public SecurityUser getUser(String username, String password) {
    return userRepository.findByUsernameAndPassword(username, password).
        map(SecurityUser::from).
        orElse(null);
  }

  public UserDTO register(RegisterUserDto registerUser) {
    User newUser = User.UserBuilder.anUser().withUsername(registerUser.getUsername()).withPassword(registerUser.getPassword()).build();
    User savedUser = userRepository.save(newUser);
    return UserDTO.builder().id(savedUser.getId()).username(savedUser.getUsername()).build();
  }


  public void updateUser(User user) throws NotFound {
    User urs = getUserByUsernameAndPassword(user.getUsername(), user.getPassword());
    urs.setToken(user.getToken());
    userRepository.save(urs);
   }

   public User getUserByUsernameAndPassword(String username, String password) throws NotFound {
      return userRepository.findByUsernameAndPassword(username, password).orElseThrow(NotFound::new);
   }
}
