package pl.psst.hopeitapp.backend.domain.donations;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.psst.hopeitapp.backend.domain.base.BaseEntity;
import pl.psst.hopeitapp.backend.domain.user.User;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "hi_donation")
public class Donation extends BaseEntity {

    @Column(name = "amount")
    private Double amount;
    @Column(name = "donation_date")
    private Date date;
    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;


    public static final class DonationBuilder {
        Long id;
        private Double amount;
        private Date date;
        private User user;

        private DonationBuilder() {
        }

        public static DonationBuilder aDonation() {
            return new DonationBuilder();
        }

        public DonationBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public DonationBuilder withAmmount(Double ammount) {
            this.amount = ammount;
            return this;
        }

        public DonationBuilder withDate(Date date) {
            this.date = date;
            return this;
        }

        public DonationBuilder withUser(User user) {
            this.user = user;
            return this;
        }

        public Donation build() {
            Donation donation = new Donation();
            donation.setId(id);
            donation.setAmount(amount);
            donation.setDate(date);
            donation.setUser(user);
            return donation;
        }
    }
}
