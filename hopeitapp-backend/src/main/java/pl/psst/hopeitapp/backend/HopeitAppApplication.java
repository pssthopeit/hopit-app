package pl.psst.hopeitapp.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;

@SpringBootApplication
@EnableJpaRepositories
public class HopeitAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HopeitAppApplication.class, args);
	}

	@Bean
	public MultipartResolver multipartResolver() {
		return new StandardServletMultipartResolver();
	}
//
//	@Bean
//	public MultipartConfigElement multipartConfigElement() {
//		MultipartConfigFactory factory = new MultipartConfigFactory();
//
//		factory.setMaxFileSize("10MB");
//		factory.setMaxRequestSize("10MB");
//
//		return factory.createMultipartConfig();
//	}
}
