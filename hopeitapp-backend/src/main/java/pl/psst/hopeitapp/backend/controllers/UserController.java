package pl.psst.hopeitapp.backend.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.psst.hopeitapp.shared.dtos.validators.ValidUserData;
import pl.psst.hopeitapp.backend.services.UserService;
import pl.psst.hopeitapp.shared.dtos.RegisterUserDto;
import pl.psst.hopeitapp.shared.dtos.user.UserDTO;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

  private final UserService userService;

  @PostMapping("/register")
  public UserDTO register(@RequestBody @ValidUserData RegisterUserDto registerUser){
    return userService.register(registerUser);
  }

}
