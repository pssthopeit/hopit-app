package pl.psst.hopeitapp.backend.security.user;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.psst.hopeitapp.backend.domain.user.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@Getter
@Setter
public class SecurityUser implements UserDetails {

  final String username;
  final String password;
  final String token;
  List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return grantedAuthorityList;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  @Override
  public String getName() {
    return username;
  }

  public static SecurityUser from(User user) {
    SecurityUser securityUser = new SecurityUser(user.getUsername(), user.getPassword(), user.getToken());
    securityUser.setGrantedAuthorityList(Collections.EMPTY_LIST);
    return securityUser;
  }
}
