package pl.psst.hopeitapp.backend.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.psst.hopeitapp.backend.services.MessageService;
import pl.psst.hopeitapp.shared.dtos.user.MessageDTO;

import javax.servlet.annotation.MultipartConfig;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@MultipartConfig()
public class MessageController {

    private final MessageService messageService;

    @GetMapping(path = "/{userId}/message",
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUserMessages(@PathVariable("userId") Long userId) {
        List<MessageDTO> messages = messageService.getUserMessages(userId);
        return messages != null ? ResponseEntity.ok(messages) :
            (ResponseEntity<?>) ResponseEntity.notFound();
    }

    @PostMapping(path = "/message", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> saveMessage(@RequestPart("msg") String msg,
        @RequestPart(value = "multipartFile", required = false) MultipartFile file, @RequestPart("ids") String ids) throws IOException {
        messageService.saveUserMessage(msg, file, ids);
        return (ResponseEntity<?>) ResponseEntity.ok();
    }
}
