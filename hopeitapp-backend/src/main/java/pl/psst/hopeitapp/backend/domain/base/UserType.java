package pl.psst.hopeitapp.backend.domain.base;

public enum UserType {
    EMPLOYEE, DONATOR
}
