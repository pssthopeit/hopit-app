package pl.psst.hopeitapp.backend.domain.converters;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import pl.psst.hopeitapp.backend.domain.message.Message;
import pl.psst.hopeitapp.backend.domain.repositories.UserRepository;
import pl.psst.hopeitapp.shared.dtos.user.MessageDTO;
import pl.psst.hopeitapp.shared.dtos.user.UserMessageDTO;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;

@Component
public class Messages {

    private final UserRepository userRepository;

    @Autowired
    protected Messages(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static Message toMessage(UserMessageDTO messageDTO) {
        return Message.MessageBuilder.aMessage().withMsg(messageDTO.getMessageDTO().getMessage())
            .withUsers(
                messageDTO.getUsers().stream().map(Users::toUser).collect(Collectors.toList()))
            .build();
    }

    public static MessageDTO toMessageDTO(Message message){
        return MessageDTO.builder().message(message.getMsg()).localDateTime(message.getLocalDateTime()).build();
    }

    public Message toMessage(String msg, MultipartFile file, String ids) throws IOException {
        return new Message(msg, userRepository.findAllById(Arrays.stream(ids.split(",")).map(Long::valueOf).collect(Collectors.toList())), LocalDateTime.now(), file.getBytes());
    }
}
