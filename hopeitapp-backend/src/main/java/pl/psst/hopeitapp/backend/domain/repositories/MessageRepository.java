package pl.psst.hopeitapp.backend.domain.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.psst.hopeitapp.backend.domain.message.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long>{
}
