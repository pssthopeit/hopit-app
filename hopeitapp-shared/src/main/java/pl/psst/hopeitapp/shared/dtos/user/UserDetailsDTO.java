package pl.psst.hopeitapp.shared.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDetailsDTO {

    private Long id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    private String address;
    @NotNull
    private String email;

}
