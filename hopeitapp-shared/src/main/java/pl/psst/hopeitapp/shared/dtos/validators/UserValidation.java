package pl.psst.hopeitapp.shared.dtos.validators;

public interface UserValidation {
    boolean exists(String username);
}
