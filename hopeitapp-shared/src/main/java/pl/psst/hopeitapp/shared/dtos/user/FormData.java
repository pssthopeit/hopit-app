package pl.psst.hopeitapp.shared.dtos.user;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class FormData {

  private MultipartFile multipartFile;
  private String msg;
  private List<Long> ids;
}
