package pl.psst.hopeitapp.shared.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DonationDTO {

    private Long id;
    private Double amount;
    private Date donationDate;
    private UserDTO userDTO;
}
