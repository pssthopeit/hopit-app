package pl.psst.hopeitapp.shared.dtos.validators;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { RegisterValidatorImpl.class })
public @interface ValidUserData {
  String message() default "User Exists";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
