package pl.psst.hopeitapp.shared.dtos.validators;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Lazy
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegisterValidatorImpl implements ConstraintValidator<ValidUserData, String> {

  private final UserValidation userValidation;

  @Override
  public void initialize(ValidUserData constraintAnnotation) {

  }

  @Override
  public boolean isValid(String registerUserDto, ConstraintValidatorContext constraintValidatorContext) {
    return !userValidation.exists(registerUserDto);
  }
}
