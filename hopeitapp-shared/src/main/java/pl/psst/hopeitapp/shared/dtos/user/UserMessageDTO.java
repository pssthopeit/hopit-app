package pl.psst.hopeitapp.shared.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserMessageDTO {

    private List<UserDTO> users;
    private MessageDTO messageDTO;

}
