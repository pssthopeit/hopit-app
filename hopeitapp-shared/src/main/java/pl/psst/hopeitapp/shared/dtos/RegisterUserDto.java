package pl.psst.hopeitapp.shared.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.psst.hopeitapp.shared.dtos.validators.ValidUserData;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterUserDto {

  @ValidUserData
  private String username;
  private String password;
  private String email;
}
